<section class="section-padding" id="section_3">
    <div class="container">
        <div class="row">

            <div class="col-lg-12 col-12 text-center mb-4">
                <h2>Our Products</h2>
            </div>

            <div class="col-lg-4 col-md-6 col-12 mb-4 mb-lg-0">
                <div class="custom-block-wrap">
                    <img src="{{asset('assets/images/causes/bottle1.jpg')}}" class="custom-block-image img-fluid" alt="">

                    <div class="custom-block">
                        <div class="custom-block-body">
                            <h5 class="mb-3">Medicine One</h5>

                            <p>Lorem Ipsum dolor sit amet, consectetur adipsicing kengan omeg kohm tokito</p>

                            <div class="progress mt-4">
                                <div class="progress-bar w-75" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>

                            <div class="d-flex align-items-center my-2">
                                <p class="mb-0">
                                    <strong>Price:</strong> $18,500
                                </p>

                                <p class="ms-auto mb-0">
                                    <strong>Promo:</strong> N/A
                                </p>
                            </div>
                        </div>

                        <a href="products" class="custom-btn btn">Buy now</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-12 mb-4 mb-lg-0">
                <div class="custom-block-wrap">
                    <img src="{{asset('assets/images/causes/bottle2.jpg')}}" class="custom-block-image img-fluid" alt="">

                    <div class="custom-block">
                        <div class="custom-block-body">
                            <h5 class="mb-3">Medicine Two</h5>

                            <p>Sed leo nisl, posuere at molestie ac, suscipit auctor mauris. Etiam quis metus tempor</p>

                            <div class="progress mt-4">
                                <div class="progress-bar w-50" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>

                            <div class="d-flex align-items-center my-2">
                                <p class="mb-0">
                                    <strong>Price:</strong> $27,600
                                </p>

                                <p class="ms-auto mb-0">
                                    <strong>Promo:</strong> N/A
                                </p>
                            </div>
                        </div>

                        <a href="products" class="custom-btn btn">Buy now</a>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-12 mb-4 mb-lg-0">
                <div class="custom-block-wrap">
                    <img src="{{asset('assets/images/causes/bottle3.jpg')}}" class="custom-block-image img-fluid" alt="">

                    <div class="custom-block">
                        <div class="custom-block-body">
                            <h5 class="mb-3">Medicine Three</h5>

                            <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus</p>

                            <div class="progress mt-4">
                                <div class="progress-bar w-100" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>

                            <div class="d-flex align-items-center my-2">
                                <p class="mb-0">
                                    <strong>Price:</strong> $84,600
                                </p>

                                <p class="ms-auto mb-0">
                                    <strong>Promo:</strong> N/A
                                </p>
                            </div>
                        </div>

                        <a href="products" class="custom-btn btn">Buy now</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mb-4 mb-lg-0">
                <div class="custom-block-wrap">
                    <img src="{{asset('assets/images/causes/bottle4.png')}}" class="custom-block-image img-fluid" alt="">

                    <div class="custom-block">
                        <div class="custom-block-body">
                            <h5 class="mb-3">Product Four</h5>

                            <p>Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus</p>
                            
                            <div class="progress mt-4">
                                <div class="progress-bar w-100" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>

                            <div class="d-flex align-items-center my-2">
                                <p class="mb-0">
                                    <strong>Price:</strong> $84,600
                                </p>

                                <p class="ms-auto mb-0">
                                    <strong>Promo:</strong> $N/A
                                </p>
                            </div>
                        </div>

                        <a href="products" class="custom-btn btn">Buy now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>