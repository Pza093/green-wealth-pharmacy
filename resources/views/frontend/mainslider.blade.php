<section class="hero-section hero-section-full-height">
    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12 col-12 p-0">
                <div id="hero-slide" class="carousel carousel-fade slide" data-bs-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{asset('assets/images/slide/factory1.webp')}}" class="carousel-image img-fluid" alt="...">

                            <div class="carousel-caption d-flex flex-column justify-content-end">
                                <h1>By the Heart</h1>

                                <p>We are producing traditional healthy suppliment in Modern way</p>
                            </div>
                        </div>

                        <div class="carousel-item ">
                            <img src="{{asset('assets/images/slide/factory2.webp')}}" class="carousel-image img-fluid" alt="...">

                            <div class="carousel-caption d-flex flex-column justify-content-end">
                                <h1>Effective</h1>

                                <p>Take our medicien and get the fresh life</p>
                            </div>
                        </div>

                        <div class="carousel-item">
                            <img src="{{asset('assets/images/slide/factory3.jpg')}}" class="carousel-image img-fluid" alt="...">

                            <div class="carousel-caption d-flex flex-column justify-content-end">
                                <h1>Non-Side-effect</h1>

                                <p>The medicine which has no side effect</p>
                            </div>
                        </div>
                    </div>

                    <button class="carousel-control-prev" type="button" data-bs-target="#hero-slide" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>

                    <button class="carousel-control-next" type="button" data-bs-target="#hero-slide" data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                </div>
            </div>

        </div>
    </div>
</section>

