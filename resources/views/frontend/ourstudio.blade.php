<section class="section-padding section-bg" id="section_2">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 col-12 mb-5 mb-lg-0">
                <img src="{{asset('assets/images/group-people-volunteering-foodbank-poor-people.jpg')}}" class="custom-text-box-image img-fluid" alt="">
            </div>

            <div class="col-lg-6 col-12">
                <div class="custom-text-box">
                    <h2 class="mb-2">Our Story</h2>

                    <h5 class="mb-3">Green Wealth Pharmacy</h5>

                    <p class="mb-0">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nisi, deleniti. Enim molestiae vero illo, consequuntur nemo, id sapiente quis dolores odit dignissimos quae, nihil nobis consectetur corporis deserunt facilis. Accusantium!</p>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-12">
                        <div class="custom-text-box mb-lg-0">
                            <h5 class="mb-3">Our Mission</h5>

                            <p>Sed leo nisl, posuere at molestie ac, suscipit auctor quis metus</p>

                            <ul class="custom-list mt-2">
                                <li class="custom-list-item d-flex">
                                    <i class="bi-check custom-text-box-icon me-2"></i> Some Highlight
                                </li>

                                <li class="custom-list-item d-flex">
                                    <i class="bi-check custom-text-box-icon me-2"></i> Some Highlight
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-12">
                    <div class="custom-text-box mb-lg-0">
                            <h5 class="mb-3">Our Mission</h5>

                            <p>Sed leo nisl, posuere at molestie ac, suscipit auctor quis metus</p>

                            <ul class="custom-list mt-2">
                                <li class="custom-list-item d-flex">
                                    <i class="bi-check custom-text-box-icon me-2"></i> Some Highlight

                                <li class="custom-list-item d-flex">
                                    <i class="bi-check custom-text-box-icon me-2"></i> Some Highlight
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
