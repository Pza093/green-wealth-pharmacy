<section class="section-padding">
        <div class="container">
            <div class="row">

                <div class="col-lg-10 col-12 text-center mx-auto">
                    <h2 class="mb-5">Green Wealth Pharmcy For Everyone</h2>
                </div>

            <div class="col-lg-3 col-md-6 col-12 mb-4 mb-lg-0">
                <div class="featured-block d-flex justify-content-center align-items-center">
                    <a href="donate.html" class="d-block">
                        <img src="{{asset('assets/images/icons/hands.png')}}" class="featured-block-image img-fluid" alt="">

                        <p class="featured-block-text">Product <strong>One</strong></p>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-12 mb-4 mb-lg-0 mb-md-4">
                <div class="featured-block d-flex justify-content-center align-items-center">
                    <a href="donate.html" class="d-block">
                        <img src="{{asset('assets/images/icons/heart.png')}}" class="featured-block-image img-fluid" alt="">

                        <p class="featured-block-text"><strong>Product</strong> Two</p>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-12 mb-4 mb-lg-0 mb-md-4">
                <div class="featured-block d-flex justify-content-center align-items-center">
                    <a href="donate.html" class="d-block">
                        <img src="{{asset('assets/images/icons/receive.png')}}" class="featured-block-image img-fluid" alt="">

                        <p class="featured-block-text">Product <strong>Three</strong></p>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-12 mb-4 mb-lg-0">
                <div class="featured-block d-flex justify-content-center align-items-center">
                    <a href="donate.html" class="d-block">
                        <img src="{{asset('assets/images/icons/scholarship.png')}}" class="featured-block-image img-fluid" alt="">

                        <p class="featured-block-text"><strong>Product</strong> Four</p>
                    </a>
                </div>
            </div>

        </div>
    </div>
</section>