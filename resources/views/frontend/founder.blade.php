<section class="about-section section-padding">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 col-md-5 col-12">
                <img src="{{asset('assets/images/ceo.jpg')}}" class="about-image ms-lg-auto bg-light shadow-lg img-fluid" alt="">
            </div>

            <div class="col-lg-5 col-md-7 col-12">
                <div class="custom-text-block">
                    <h2 class="mb-0">Mr. Kyaw Win</h2>

                    <p class="text-muted mb-lg-4 mb-md-4">Founder / CEO</p>

                    <p>Lorem Ipsum dolor sit amet, consectetur adipsicing kengan omeg kohm tokito Professional charity theme based</p>

                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Pariatur provident voluptatem quaerat rem eum maiores placeat, voluptas officia! Ex doloremque accusamus reiciendis eos blanditiis ab eius, vel beatae exercitationem consectetur!</p>

                    <ul class="social-icon mt-4">
                        <li class="social-icon-item">
                            <a href="#" class="social-icon-link bi-twitter"></a>
                        </li>

                        <li class="social-icon-item">
                            <a href="#" class="social-icon-link bi-facebook"></a>
                        </li>

                        <li class="social-icon-item">
                            <a href="#" class="social-icon-link bi-instagram"></a>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</section>
