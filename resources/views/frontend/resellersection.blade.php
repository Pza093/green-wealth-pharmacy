<section class="reseller-section section-padding" id="section_4">
    <div class="container">
        <div class="row">

            <div class="col-lg-6 col-12">
                <h2 class="text-white mb-4">Reseller</h2>

                <form class="custom-form reseller-form mb-5 mb-lg-0" action="#" method="post" role="form">
                    <h3 class="mb-4">Become a Reseller today</h3>

                    <div class="row">
                        <div class="col-lg-6 col-12">
                            <input type="text" name="reseller-name" id="reseller-name" class="form-control" placeholder="Jack Doe" required>
                        </div>

                        <div class="col-lg-6 col-12">
                            <input type="email" name="reseller-email" id="reseller-email" pattern="[^ @]*@[^ @]*" class="form-control" placeholder="Jackdoe@gmail.com" required>
                        </div>

                        <div class="col-lg-6 col-12">
                            <input type="text" name="reseller-subject" id="reseller-subject" class="form-control" placeholder="Subject" required>
                        </div>

                        <div class="col-lg-6 col-12">
                            <div class="input-group input-group-file">
                                <input type="file" class="form-control" id="inputGroupFile02">

                                <label class="input-group-text" for="inputGroupFile02">Upload your CV</label>

                                <i class="bi-cloud-arrow-up ms-auto"></i>
                            </div>
                        </div>
                    </div>

                    <textarea name="reseller-message" rows="3" class="form-control" id="reseller-message" placeholder="Comment (Optional)"></textarea>

                    <button type="submit" class="form-control">Submit</button>
                </form>
            </div>

            <div class="col-lg-6 col-12">
                <img src="{{asset('assets/images/smiling-casual-woman-dressed-volunteer-t-shirt-with-badge.jpg')}}" class="reseller-image img-fluid" alt="">

                <div class="custom-block-body text-center">
                    <h4 class="text-white mt-lg-3 mb-lg-3">About Reselling</h4>

                    <p class="text-white">Lorem Ipsum dolor sit amet, consectetur adipsicing kengan omeg kohm tokito Professional charity theme based</p>
                </div>
            </div>

        </div>
    </div>
</section>